﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WordAutoSuggestion
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            List<string> matchList = new List<string>();
            Words words = new Words();
            bool choice=true;
            string input, input_new;
            while (choice == true)
            {

                matchList.Clear();
                stopwatch.Reset();

                Console.WriteLine("\nEnter a word: ");
                 input = Console.ReadLine();
                Console.WriteLine();


                 input_new = input + "[a-zA-Z]*";
                bool flag = false;

                Regex regex = new Regex(@"\b" + input_new, RegexOptions.IgnoreCase);

                stopwatch.Start();


                foreach (string i in words.wordList)
                {
                    Match match = regex.Match(i);

                    if (match.Success)
                    {

                        matchList.Add(match.Value);
                      //  Console.WriteLine(match.Value);
                        flag = true;
                       
                      

                    }

                }

                stopwatch.Stop();

                if(flag == true)
                {
                    Console.WriteLine("\n------Suggestions-----\n");

                    foreach (var item in matchList)
                    {
                        Console.WriteLine(item);
                    }
                }


                if (flag == false)
                {
                    Console.WriteLine("\nNo Suggestion");
                    //words.wordList.Add(input);

                }

                Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed.Milliseconds.ToString());


                Console.WriteLine("Continue? (y/n)");
                string ask = Console.ReadLine();

                if (ask == "n")
                    choice = false;
            }



            Console.ReadKey();

        }
    }
}
