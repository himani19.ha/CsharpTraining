﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionPaper
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            QuestionSet questionSet = new QuestionSet();

            questionSet.GenerateQuestions();

            // Begin timing.
            stopwatch.Start();

            questionSet.GenerateQP();

            // Stop timing.
            stopwatch.Stop();

            Console.WriteLine("----Question Paper----");
            questionSet.PrintQP();


            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed.Milliseconds.ToString());

            Console.ReadKey();

        }
    }
}
