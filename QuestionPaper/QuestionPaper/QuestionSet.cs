﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionPaper
{
    public class QuestionSet
    {
        //public List<string> A = new List<string> {"A1","A2","A3"};
        //public List<string> B = new List<string> {"B1","B2","B3" };
        //public List<string> C = new List<string> { "C1", "C2", "C3","C4","C5","C6","C7","C8","C9","C10"};
        //public List<string> D = new List<string> { "D1", "D2", "D3","D4" };

        public List<string> A = new List<string>();
        public List<string> B = new List<string>();
        public List<string> C = new List<string>();
        public List<string> D = new List<string>();

        public List<string> FinalList = new List<string>();

        public int count1, count2, count3, count4, total_count;

        public void GenerateQuestions()
        {
            for (int i = 1; i <= 10000; i++)
                A.Add("A" + i);

            for(int i = 1; i <= 20000; i++)
                B.Add("B" + i);

            for(int i = 1; i <= 7000; i++)
                C.Add("C" + i);

            for(int i = 1; i <= 13000; i++)
                D.Add("D" + i);
        }

        public int FindMax(int[] arr )
        {
            int max = -1;
            int index = 0;

            for(int i=0;i<4;i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    index = i;

                }

            }
            return index ;
        }

        public int FindSecondMax(int[] arr)
        {
            int maxIndex = FindMax(arr);
            int max = -1;
            int index = 0;

            for (int i = 0; i < 4; i++)
            {
                if (i == maxIndex)
                    continue;

                else if (arr[i] > max)
                {
                    max = arr[i];
                    index = i;

                }

            }
            return index;

        }

        public void GenerateQP()
        {

            count1 = A.Count;
            count2 = B.Count;
            count3 = C.Count;
            count4 = D.Count;

            total_count = count1 + count2 + count3 + count4;
            int aIndex = 0, bIndex = 0, cIndex = 0, dIndex = 0;


            int[] CountArray = new int[4] {count1,count2,count3,count4 };
            int prevIndex = 0;

            for(int i=0; i<total_count; i++)
            {
                int index=FindMax(CountArray);

                if (prevIndex == index)
                    index = FindSecondMax(CountArray);

                CountArray[index]--;


                if(index==0)
                {
                    //Console.WriteLine( " "+A[aIndex]);
                    FinalList.Add(A[aIndex]);
                    aIndex++;
                }

                else if(index==1)
                {
                    //Console.WriteLine( " " + B[bIndex]);
                    FinalList.Add(B[bIndex]);
                    bIndex++;
                }

                else if (index == 2)
                {
                    //Console.WriteLine( " " + C[cIndex]);
                    FinalList.Add(C[cIndex]);
                    cIndex++;
                }

                else if (index == 3)
                {
                    //Console.WriteLine(" "+ D[dIndex]);
                    FinalList.Add(D[dIndex]);
                    dIndex++;
                }

                prevIndex = index;
            }

        }

        public void PrintQP()
        {
            foreach(string item in FinalList)
                Console.WriteLine(item);
        }

    }
}
